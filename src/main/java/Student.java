
public class Student extends Person {

	private String facultyNumber;
	
	public Student(String name, int age, String facultyNumber) {
		super(name, age);
		setFacultyNumber(facultyNumber);
		
	}
	
	public String getFacultyNumber() {
		return facultyNumber;
	}
	
	public void setFacultyNumber(String facultyNumber) {
		this.facultyNumber = facultyNumber;
	}

}
